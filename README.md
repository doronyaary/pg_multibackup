# ![](pgm.webp) &nbsp;&nbsp; pg_multibackup

The pg_multibackup is an easy tool to centralize backups from WAL fetching (using pg_receivewal) from a central location. It can backup multiple postgresql clusters to a central location and provide split-brain detection as well. It was built to provide extreme simplicity. pg_multibackup provides the following capabilities:

- Remote backups 
- Centralized backup location
- Split-brain detection and failure
- Automatic replication failover detection (forcing new base backup)
- Backup files are always at your disposal

## Installation

Installation is fairly easy. Follow the below steps to install pg_multibackup on your system. The following steps are done and tested on a RHEL system (8.x) with PostgreSQL 14 but should work with any version above 12 (Ubuntu steps will be provided later).

### Add PostgreSQL repository & install PostgreSQL client

```bash
dnf install https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
dnf -q module disable postgresql
dnf install postgresql14
```

### Install Python3 requirements

pg_multibackup requires some python3 installations and dependencies. Should be an easy install.

```bash
dnf install epel-release
dnf install python3 python3-pip 
dnf install python3-psycopg2
pip3 install configparser
```

### Install pg_multibackup

The repository holds the compiled version of pg_multibackup

```bash
git clone https://gitlab.com/doronyaary/pg_multibackup.git
cd pg_multibackup
cd <VERSION>
cp pg_multibackup /usr/pgsql-14/bin/
```
**Important:**
The "VERSION" is refering to the latest version in the branch. This git holds every binary from every version within it. This is the easiest way that one can always have the binaries according to his/hers demands.

### Verify executables

pg_multibackup uses both pg_basebackup and pg_receivewal in order to do its job. We need to verify that these tools are visible in the user's path. Please make sure to check this with the user that will run pg_multibackup. 

```bash
which pg_basebackup
which pg_receivewal
```

BTW: If not all are in your path, please consider adding it to your ".bash_profile" configuration.

## pg_multibackup Configuration 

The configuration file is a single INI file containing two key sections. The first one is called "globals" and is a mandatory section. The second one (or actually any other else) is a section-per-cluster, which means that each PostgreSQL cluster/server is written here with its backup preferences so pg_multibackup can know what to do. Please see the following example for the "globals" section:

```INI
[globals]
home=/backupmount/
```

These are the key(s) for the "globals" section:

| Key | Description |
| - | - |
| home | Provides the name of the backup location/mount where to keep all backup files/data |

For each cluster/server, you would need to add an additional section that will identify the cluster (remember: section names must be unique). Each cluster section will contain the following keys:

| Key | Mandatory | Description |
| - | - | - |
| hosts | Yes | The hosts and ports of the servers composing the replication cluster. For example: 10.0.0.1:5432,10.0.0.2:5432 etc. |
| user | Yes | The PostgreSQL user that will be used to perform base backups and pg_receivewal |
| basetime | Yes | The time, in the format of "HHMM" (2-hour 2-minutes) to run base backups. For example, if you want to backup the cluster every day at 23:00PM you should use "2300" as your value. |
| retention | Yes | A numeric value that determines how many base backup sets (base + WALs) to keep |
| waltimer | No | When to run pg_receivewal - in terms of every X minutes. If you write 5, that means that pg_multibackup will run pg_receivewal every 5 minutes |
| password | No | The password for the user that performs the backups. If not specified, pg_multibackup will try to rely on the ".pgpass" file |
| slot | No | The replication slot name to use so the cluster will not remove needed WAL segments |
| prescript | No | The bash command that will be executed before the base backup starts |
| postscript | No | The bas command that will be executed after the base backup finishes (regardless of success or failure) |

This would be a typical "base-only" section configuration:

```INI
[staging_cluster]
hosts=10.0.0.1:5432,10.0.0.2:5432,10.0.0.3:5432
user=repl
password=repl_password_here
basetime=2200
retention=2
```

This would be a typical configuration section for a "base and WALs" that runs pg_receivewal every 10 minutes:

```INI
[staging_cluster]
hosts=10.0.0.1:5432,10.0.0.2:5432,10.0.0.3:5432
user=repl
password=repl_password_here
basetime=2200
waltimer=10
retention=2
slot=stg_repl_slot
```
### Pre/Post script details

You have the possibility to run a script before and after every base backup, also called the "pre" script and the "post" script. In order to help you use the script better, pg_multibackup will pass along the following details as environment variables - both to the "pre" and to the "post":

| Variable | Description |
| - | - |
| PGM_PGHOST | The PostgreSQL host that the backup was taken from |
| PGM_PGPORT | The PostgreSQL port that the backup was taken from |
| PGM_BASEFOLDER | The folder name that the base backup resides on |

**Important Note:**
Please remember that the bash script is being executed as the user that runs the pg_multibackup daemon. Plan accordingly.

### Running pg_multibackup

pg_multibackup is actually a daemon that runs without exiting. pg_multibackup accepts three command line arguments:

| Argument | Mandatory | Description/Notes |
| - | - | - |
| --config | Yes | The congiuration file (INI) for pg_multibackup |
| --log | No | The log file name to write information/warnings/errors to |
| --silent | No | No CLI output - just log to file |

Once you configured the INI file, a typical execution of the daemon would look like this:

```bash
pg_multibackup --config=/backup/pg_multibackup.ini --log=/backup/pg_multibackup.log
```

In most cases, and especially when you will create a unit file (e.g. service) for it, you wouldn't want a CLI output. In RHEL linux, this would be a simple unit file example for use with systemctl (the file/unit name would probably be "/etc/systemd/system/pg_multibackup.service)

```bash
[Unit]
Description=pg_multibackup

[Service]
ExecStart=/usr/pgsql-14/bin/pg_multibackup --config=/backup/pg_multibackup.ini --log=/backup/pg_multibackup.log --silent

[Install]
WantedBy=multi-user.target
```

## Releases

Up until version 1.4.0, pg_multibackup was a set of bash scripts that needed crontab adjustments. While the bash scripts are perfectly fine and technically operational, I've decided to "wrap" it and serve it as a single binary with a configuration file.

| Version | Released | Information/Details |
| - | - | - |
| 1.4.0 | 2023-05-20 | First version as a single binary with a configuration file |



